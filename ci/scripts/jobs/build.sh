
citbx_use "dind"

job_define() {
    bashopts_declare -n DOCKER_TAG -l docker-tag -t string \
        -d "Set Docker tag"
    citbx_export DOCKER_TAG
}

job_main() {
    : ${DOCKER_TAG:=${CI_COMMIT_REF_NAME##*/}}
    : ${DOCKER_TYPE:=${DOCKER_TAG##*-}}
    : ${DOCKER_VERSION:=${DOCKER_TAG%-*}}
    case "$DOCKER_TYPE" in
        dind|git)
            DOCKER_FROM="docker:$DOCKER_TAG"
            ;;
        extra)
            DOCKER_FROM="docker:$DOCKER_VERSION-git"
            ;;
        *)
            print_critical "Unsupported docker image (tag='$DOCKER_TAG')"
            ;;
    esac
    cat Dockerfile.d/*-$DOCKER_TYPE* > Dockerfile
    docker build -t $CI_REGISTRY_IMAGE:$DOCKER_TAG \
        --build-arg DOCKER_FROM=$DOCKER_FROM \
        --build-arg DOCKER_VERSION=$DOCKER_VERSION \
        .
    if [ -n "$CI_JOB_TOKEN" ]; then
        docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" $CI_REGISTRY
        docker push $CI_REGISTRY_IMAGE:$DOCKER_TAG
    fi
}

job_after() {
    local retcode=$1
    rm -f Dockerfile
    if [ $retcode -eq 0 ]; then
        print_info "Image \"$CI_REGISTRY_IMAGE:$DOCKER_TAG\" successfully generated"
    fi
}


citbx_module_dind_define() {
    bashopts_declare -n USE_DIND_SERVICE -l use-dind -d "Use the dind docker service instead of the host docker instance" -t boolean
}

citbx_module_dind_setup() {
    if [ $CITBX_UID -ne 0 ]; then
        DOCKER_GID=$(awk -F ':' '/docker:/ {print $3; exit;}' /etc/group)
        citbx_export DOCKER_GID
    fi
    CITBX_USER_GROUPS+=(docker)
    if [ "$USE_DIND_SERVICE" == "false" ]; then
        CITBX_DISABLED_SERVICES+=(docker)
    fi
    pattern='\bdocker\b'
    if [[ "${CITBX_DISABLED_SERVICES[*]}" =~ $pattern ]]; then
        print_info "Using the host docker instance instead of docker dind service one (CITBX_JOB_ACCESS_TO_HOST_DOCKER_SOCK_ENABLED option forced at true)"
        DOCKER_HOST="unix:///var/run/docker.sock"
        CITBX_JOB_ACCESS_TO_HOST_DOCKER_SOCK_ENABLED="true"
        citbx_export DOCKER_HOST
    fi
}

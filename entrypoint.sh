#!/bin/sh -e

write_ca_certs() {
    local dir=$1
    mkdir -p "$dir"
    (echo -e "$CI_SERVER_TLS_CA_FILE" \
        && echo -e "$PROJECT_ADD_TLS_CA_FILE") | awk -v n=0 '
        /^\s*$/ {
            next;
        }
        {
            print > "'"$dir"'/custom-ca-cert-" n ".crt"
        }
        /-----END CERTIFICATE-----/ {
            n++;
        }'
}

if [ -n "$CI_SERVER_TLS_CA_FILE" ] || [ -n "$PROJECT_ADD_TLS_CA_FILE" ]; then
    write_ca_certs /usr/local/share/ca-certificates/
    if ! update-ca-certificates 2>> /var/log/update-ca-certificates.log ; then
        echo "update-ca-certificates failed!"
        cat /var/log/update-ca-certificates.log
    fi
    mkdir -p /etc/docker/certs.d
    cp /usr/local/share/ca-certificates/*.crt /etc/docker/certs.d/
fi

if ! grep -q '^docker:' /etc/group; then
    addgroup -S docker
fi

# Add a docker group with a specific GID
if [ -n "$DOCKER_GID" ]; then
    # Same as `addgroup -g $DOCKER_GID docker` ignoring duplicate GID
    sed -E -i 's/^docker:x:\d+:/docker:x:'"$DOCKER_GID"':/g' /etc/group
fi

eval "$(printenv | awk -F '=' '
    /^DOCKERD_ARG_[^=]+=/ {
        arg_name = tolower(substr($1, 13));
        gsub("_", "-", arg_name);
        arg_val = ENVIRON[$1];
        gsub("'"'"'", "'"'\\\"'\\\"'"'", arg_val)
        printf("set -- \"$@\" '"'"'--%s=%s'"'"'", arg_name, arg_val);
    }
')"

exec /usr/local/bin/$0 "$@"
